# Disco Package - Tables

This package contains all Disco's tables.

## Project

Some utils commands.

```shell script
$ yarn install
$ yarn serve
$ yarn build
$ yarn lint
$ yarn test
```

## Information

You can access to the wiki [here](https://gitlab.com/shiros/disco/tables/-/wikis/home).

In this project, we'll use some external libs.

### Disco

- [Services](https://gitlab.com/shiros/disco/services), you can see the
  licence [here](https://gitlab.com/shiros/disco/services/-/blob/master/LICENSE).

### Community

- [Uuid](https://github.com/uuidjs/uuid), you can see the
  licence [here](https://github.com/uuidjs/uuid/blob/master/LICENSE.md).
- [VueJs](https://github.com/vuejs/vue), you can see the
  licence [here](https://github.com/vuejs/vue/blob/master/LICENSE).

## Installation

Follow the command below, to install this package :

```shell
$ yarn add disco-tables
OR
$ yarn add ssh://git@gitlab.com:shiros/disco/tables.git#<branch_name>
```

## Authors

- [Alexandre Caillot (Shiroe_sama)](https://gitlab.com/Shiroe_sama)
